const assert = require('assert');
const Sudoku = require('../actions/sudoku.js');

const SudokuObj = new Sudoku();

it('should return true', () => {
  assert.strictEqual(true, true);
});

const input1 = [
  1, 0, 0, 4, 8, 9, 0, 0, 6,
  7, 3, 0, 0, 0, 0, 0, 4, 0,
  0, 0, 0, 0, 0, 1, 2, 9, 5,
  0, 0, 7, 1, 2, 0, 6, 0, 0,
  5, 0, 0, 7, 0, 3, 0, 0, 8,
  0, 0, 6, 0, 9, 5, 7, 0, 0,
  9, 1, 4, 6, 0, 0, 0, 0, 0,
  0, 2, 0, 0, 0, 0, 0, 0, 0,
  8, 0, 0, 5, 1];

const input2 = [
  1, 0, 0, 4, 8, 9, 0, 0, 6,
  7, 3, 0, 0, 0, 0, 10, 4, 0,
  0, 0, 0, 0, 0, 1, 2, 9, 5,
  0, 0, 7, 1, 2, 0, 6, 0, 0,
  5, 0, 0, 7, 0, 3, 0, 0, 8,
  0, 0, 6, 0, 9, 5, 7, 0, 0,
  9, 1, 4, 6, 0, 0, 0, 0, 0,
  0, 2, 0, 0, 0, 0, 0, 0, 0,
  8, 0, 0, 5, 1, 2, 0, 0, 4];

const input3 = [
  1, 0, 0, 4, 8, 9, 0, 0, 6,
  7, 3, 0, 0, 0, 0, 0, 4, 0,
  0, 0, 0, 0, 0, 1, 2, 9, 5,
  0, 0, 7, 1, 2, 0, 6, 0, 0,
  5, 0, 0, 7, 0, 3, 0, 0, 8,
  0, 0, 6, 0, 9, 5, 7, 0, 0,
  9, 1, 4, 6, 0, 0, 0, 0, 0,
  0, 2, 0, 0, 0, 0, 0, 0, 0,
  8, 0, 0, 5, 1, 2, 0, 0, 4];

it('should check input data', () => {
  const res1 = SudokuObj.initialize(input1);
  const res2 = SudokuObj.initialize(input2);
  const res3 = SudokuObj.initialize(input3);
  assert.strictEqual(res1, "ENTERED ONLY 77 DATA'S");
  assert.strictEqual(res2, 'ENTERED VALUE 10 AT POSITION 15');
  assert.strictEqual(res3.cells[79].possibleValues[0], 3);
  assert.strictEqual(res3.cells[79].possibleValues[1], 6);
  assert.strictEqual(res3.cells[79].possibleValues[2], 7);
});
