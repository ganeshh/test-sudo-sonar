
class Cube {
  constructor(position) {
    this.cubeIndex = position;
    this.cubeCells = [];
    this.possibleValues = [1, 2, 3, 4, 5, 6, 7, 8, 9];
  }

  removePossibleValueFromCellInCube(i, input, thisObject, cube) {
    const cubeObject = cube;

    cubeObject.possibleValues = cubeObject.possibleValues.filter(val => val !== input[i]);
    console.log(`CUBE INDEX : ${thisObject.cells[i].cubeIndex}
    POSSIBLE VALUES - CUBES ${(thisObject.cubes[thisObject.cells[i].cubeIndex].possibleValues)}`);
    return this;
  }

  removePossibleValueFromAssociatedCellsInCube(i, input, thisObject, cube) {
    const cubeCellsObject = cube;

    for (let rc = 0; rc < 9; rc += 1) {
      cubeCellsObject.cubeCells[rc].possibleValues = cubeCellsObject.cubeCells[rc]
        .possibleValues.filter(value => (value !== input[i]));
      console.log(thisObject.cubes[thisObject.cells[i].cubeIndex].cubeCells[rc]);
    }
    return this;
  }
}

module.exports = Cube;
