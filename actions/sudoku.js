const Cell = require('./cell.js');
const Row = require('./row.js');
const Column = require('./column.js');
const Cube = require('./cube.js');

class Sudoku {
  constructor() {
    this.createCells();
    this.createRows();
    this.createColumns();
    this.createCubes();
  }

  createCells() {
    const cells = [];
    for (let i = 0; i < 81; i += 1) {
      const newCell = new Cell(i);
      cells.push(newCell);
      console.log(`CELL POSITION : ${i} CELL VALUE : ${newCell.value}`);
    }

    this.cells = cells;
    console.log('!!..CELLS CREATED..!!');
    return this;
  }

  createRows() {
    const rows = [];
    for (let rowIndex = 0; rowIndex < 9; rowIndex += 1) {
      const newRow = new Row(rowIndex);
      for (let cellIndex = rowIndex * 9; cellIndex < (rowIndex * 9) + 9; cellIndex += 1) {
        this.cells[cellIndex].rowIndex = newRow.rowIndex;
        newRow.rowCells.push(this.cells[cellIndex]);
        console.log(`ROW INDEX : ${newRow.rowIndex}
        CELL INDEX : ${this.cells[cellIndex].position}`);
      }
      rows.push(newRow);
    }
    this.rows = rows;
    console.log('ROWS CREATED');
    return this;
  }

  createColumns() {
    const columns = [];
    for (let columnIndex = 0; columnIndex < 9; columnIndex += 1) {
      const newCol = new Column(columnIndex);
      for (let cellIndex = columnIndex; cellIndex < 81; cellIndex += 9) {
        this.cells[cellIndex].columnIndex = newCol.columnIndex;
        newCol.columnCells.push(this.cells[cellIndex]);
        console.log(`COLUMN INDEX : ${newCol.columnIndex}
        CELL INDEX : ${this.cells[cellIndex].position}`);
      }
      columns.push(newCol);
    }
    this.columns = columns;
    console.log('COLUMNS CREATED');
    return this;
  }

  createCubes() {
    const Cubes = [];
    for (let cubeIndex = 0; cubeIndex < 9; cubeIndex += 1) {
      const newCube = new Cube(cubeIndex);
      for (let cellId = 0; cellId < 9; cellId += 1) {
        const cubeIndexValue = Math.floor((cubeIndex / 3)) * 27 + ((cubeIndex % 3) * 3);
        const cellIdValue = (Math.floor(cellId / 3)) * 9 + (cellId % 3);
        const cubeCell = cubeIndexValue + cellIdValue;
        this.cells[cubeCell].cubeIndex = newCube.cubeIndex;
        newCube.cubeCells.push(this.cells[cubeCell]);
        console.log(`CUBE INDEX : ${newCube.cubeIndex}
        CELL INDEX : ${this.cells[cubeCell].position}`);
      }
      Cubes.push(newCube);
    }
    this.cubes = Cubes;
    console.log('CUBES CREATED');
    return this;
  }

  initialize(input) {
    if (input.length === 81) {
      for (let i = 0; i < 81; i += 1) {
        const res = this.validate(i, input);
        if (res) {
          return `ENTERED VALUE ${input[i]} AT POSITION ${i}`;
        }
      }
      return this;
    }
    console.log('INVALID INPUT DATA - NEED 81 DATA TO SOLVE');
    console.log(`ENTERED ONLY ${input.length} DATA'S`);
    return `ENTERED ONLY ${input.length} DATA'S`;
  }

  validate(i, input) {
    if (input[i] >= 0 && input[i] < 10) {
      if (input[i] === 0) {
        console.log('NO NEED TO REMOVE POSSIBLE VALUE');
      } else {
        this.cells[i].insertValueToCell(i, input, this);
      }
      return false;
    }
    console.log('INVALID INPUT DATA - INPUT VALUE SHOULD BE BETWEEN 0 TO 9');
    console.log(`ENTERED VALUE ${input[i]} AT POSITION ${i}`);
    return true;
  }
}

module.exports = Sudoku;
