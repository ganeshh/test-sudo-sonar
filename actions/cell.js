
class Cell {
  constructor(position) {
    this.position = position;
    this.value = null;
    this.possibleValues = [1, 2, 3, 4, 5, 6, 7, 8, 9];
    this.rowIndex = null;
    this.columnIndex = null;
    this.cubeIndex = null;
  }

  insertValueToCell(i, input, thisObject) {
    const sudokuObject = thisObject;
    sudokuObject.cells[i].value = input[i];
    console.log('####', sudokuObject.cells[i].value);
    sudokuObject.cells[i].possibleValues = [];
    this.removePossibleValueFromAssociatedCells(i, input, sudokuObject);
    this.removePossibleValueFromCell(i, input, sudokuObject);
  }

  // insertValueToCell2(i, input, thisObject2) {
  //   const sudokuObject2 = thisObject2;
  //   sudokuObject2.cells[i].value = input[i];
  //   console.log('####', sudokuObject2.cells[i].value);
  //   sudokuObject2.cells[i].possibleValues = [];
  //   this.removePossibleValueFromAssociatedCells(i, input, sudokuObject2);
  //   this.removePossibleValueFromCell(i, input, sudokuObject2);
  // }

  removePossibleValueFromCell(i, input, thisObject) {
    const sudokuObject = thisObject;
    const rowIndex = sudokuObject.rows[sudokuObject.cells[i].rowIndex];
    const columnIndex = sudokuObject.columns[sudokuObject.cells[i].columnIndex];
    const cubeIndex = sudokuObject.cubes[sudokuObject.cells[i].cubeIndex];

    rowIndex.removePossibleValueFromCellInRow(i, input, sudokuObject, rowIndex);
    columnIndex.removePossibleValueFromCellInColumn(i, input, sudokuObject, columnIndex);
    cubeIndex.removePossibleValueFromCellInCube(i, input, sudokuObject, cubeIndex);
    return this;
  }

  removePossibleValueFromAssociatedCells(i, input, thisObject) {
    const sudokuObject = thisObject;
    const rowIndex = sudokuObject.rows[sudokuObject.cells[i].rowIndex];
    const columnIndex = sudokuObject.columns[sudokuObject.cells[i].columnIndex];
    const cubeIndex = sudokuObject.cubes[sudokuObject.cells[i].cubeIndex];

    rowIndex.removePossibleValueFromAssociatedCellsInRow(i, input, sudokuObject, rowIndex);
    columnIndex.removePossibleValueFromAssociatedCellsInColumn(i, input, sudokuObject, columnIndex);
    cubeIndex.removePossibleValueFromAssociatedCellsInCube(i, input, sudokuObject, cubeIndex);
    return this;
  }
}

module.exports = Cell;
